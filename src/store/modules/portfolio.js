const state = {
    funds: 10000,
    stocks: []
};

const mutations = {
    'BUY_STOCK'(state, {stockId, quantity, stockPrice, type}) {
        const record = state.stocks.find(element => element.id == stockId);
        if (record) {
            record.quantity += quantity;
        } else {
            state.stocks.push({
                id: stockId,
                quantity: quantity,
                time: new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds(),
                date: (new Date()).toLocaleDateString(),
                type: type
            });
        }
        // state.date = date
        // state.time = time
        state.funds -= stockPrice * quantity;
    },
    'SELL_STOCK' (state, {stockId, quantity, stockPrice}) {
        const record = state.stocks.find(element => element.id == stockId);
        if (record.quantity > quantity) {
            record.quantity -= quantity;
        } else {
            state.stocks.splice(state.stocks.indexOf(record), 1);
        }
        state.funds += stockPrice * quantity;
    },
    'SET_PORTFOLIO' (state, portfolio) {
        state.funds = portfolio.funds;
        state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : [];
    }
};

const actions = {
    sellStock({commit}, order) {
        commit('SELL_STOCK', order);
    }
};

const getters = {
    stockPortfolio (state, getters) {
        return state.stocks.map(stock => {
            const record = getters.stocks.find(element => element.id == stock.id);
            return {
                id: stock.id,
                quantity: stock.quantity,
                name: record.name,
                price: record.price,
                // date: (new Date()).toLocaleDateString(),
                date: stock.date,
                // time: new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()
                time: stock.time
            }
        });
    },
    stockTransactions (state, getters) {
        console.log("transactionstock:::", state.stocks)
        return state.stocks.map(stock => {
            const record = getters.stocks.find(element => element.id == stock.id);
            return {
                id: stock.id,
                quantity: stock.quantity,
                name: record.name,
                price: record.price,
                date: stock.date,
                time: stock.time,
                type: stock.type           
             }
        });
    },
    stockEmpty (state) {
        state.stocks = []
        console.log("stock empty called", state.stocks)
    },
    funds (state) {
      return state.funds;
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}